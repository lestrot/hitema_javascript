import Post from "../models/Post";


class PostsController{
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            let post = await Post.create({
                title: req.body.title,
                content: req.body.content,
                userId: req.body.userId
            });
            body = {
                'message': `Post ${post.title} created`,
                post
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let post = await Post.find().populate('userId');

            body = {
                post,
                'message': 'Post list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let post = await Post.findById(id);

            body = {
                'message': `Detail from ${post.title}`,
                post
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let post = await Post.findById(req.params.id);

            // Methode 3
            await post.update({
                title: req.body.title,
                content: req.body.content,

            });

            body = {
                'message': `post updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            //let id = req.params.id;
            //let post = await Post.findByIdAndRemove(id);

            await Post.remove({_id: req.params.id});
            body = {
                'message': `post deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default PostsController;