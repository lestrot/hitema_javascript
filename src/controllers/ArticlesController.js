import Article from "../models/Article";
import User from "../models/User";

class ArticlesController{
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            let article = await Article.create({
                title: req.body.title,
                content: req.body.content,
            });
            body = {
                'message': `Article ${article.title} created`,
                article
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let articles = await Article.find().select('title');

            body = {
                articles,
                'message': 'Article list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let article = await Article.findById(id);

            body = {
                'message': `Detail from ${article.title}`,
                article
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // Methode 3
            await Article.update({
                title: req.body.title,
                content: req.body.content,
            });

            body = {
                'message': `article updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let article = await Article.findByIdAndRemove(id);

            //await User.remove({_id: req.params.id});
            body = {
                'message': `article deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default ArticlesController;