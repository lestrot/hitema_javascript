import Userser from '../models/User';
import User from "../models/User";
import jwt from "jsonwebtoken";

class UserController{

    /**
     * Create User info Database
     * @param req
     * @param res
     * @returns {Promise<*>}
     */

    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            let user = await User.create({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
                userRole: req.body.userRole
            });
            body = {
                'message': `User ${user.firstname} created`,
                user
            }
        }
        catch (error) {
                status = 500;
                body = {'message': error.message};
        }
        return res.status(status).json(body);

    }

    static async auth(req,res){
        let status = 200;
        let body = {};
        try {
            // checker si l'utilisateur a le bon mot de passe et email
            let user = await User.findOne({email: req.body.email});
            console.log(req.body.email);
            console.log('------');

            console.log(user);
            console.log('------');
            console.log(user.password === req.body.password);
            if(user && user.password === req.body.password){
                let token = jwt.sign({sub: user._id}, "monsecret");
                body = {
                    user,
                    token
                }
            }
            /**
             * if(user && user.password === req.body.password)
             */
            // let token = jwt.sign({sub: user._id}, "monsecret");
            // body = {
            //     'message': `User authentificated`,
            //     user,
            //     token
            // }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res){
        let status = 200;
        let body = {};

        try {
            // récupération de tout le contenu simplement
            //let users = await User.find();

            // exemple de limitation de résultat, ici on affiche uniquement les prénom
            //let users = await User.find().select('firstname');

            // exemple de limitation en RETIRANT un champs du résultat
            let users = await User.find().select('firstname');

            // find()
            // findById()
            // findOne({email: 'content'})


            // v1 avec du contenu fixe
            // let users = [
            //     {
            //         name: "Julien"
            //     },
            //     {
            //         name: "Julien"
            //     },
            //     {
            //         name: "Julien"
            //     }
            // ];

            body = {
                users,
                'message': 'User list'
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            //id ci dessus représente la valeur :id dans la route users/:id , on peut le changer pour n'importe qu'elle valeur

            let user = await User.findById(id);
            let posts = await User.find({userId: id});
            body = {
                'message': `Detail from ${user.firstname}`,
                user,
                posts
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try {

            // // Method 1
            // let user = await User.findById(req.params.id);
            // delete req.body.password;
            // Object.assign(user, req.body);
            // await user.save();

            // // Methode 2
            // await User.findByIdAndUpdate({_id: req.params.id}, {$set: req.body});
            //let user = await User.findByIdAndUpdate(req.params.id,  req.body, {new: true});

            // Methode 3
            await User.update({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                addresses: req.body.addresses
            });

            body = {
                'message': `profile updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findByIdAndRemove(id);
            //await User.remove({_id: req.params.id});
            body = {
                'message': `user deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default UserController;