import mongoose from 'mongoose';

const commentSchema = mongoose.Schema({
    content: !String,
    createAt: {
        type: Date,
        default: Date.now()
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    articleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Article'
    }
});

const Comment = mongoose.model('Comment', commentSchema);
export default Comment;