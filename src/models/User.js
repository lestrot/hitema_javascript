import { Schema, model } from 'mongoose';
import mongoose from 'mongoose';

// Création du schéma du model
//version prof
const userSchema = new mongoose.Schema({

    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
            required: true
    },
    email: {
        type: String,
            required: true,
            unique: true
    },
    password: {
        type: String,
    },
    userRole: {
        type: Number,
        default: 1
    },
    addresses: [{
        address: String,
        city: String,
        zip_code: String
    }]
});

//Version daniel
// const userSchema = mongoose.Schema({
//     firstname: !String,
//     lastname: !String,
//     email: {
//         type: String,
//         require: true,
//         unique: true
//     },
//     addresses: [{
//         address: String,
//         city: String,
//         zip_code: String
//     }]
// });

const User = mongoose.model('User', userSchema);
export default User;

//User user