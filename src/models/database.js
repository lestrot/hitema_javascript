// on importe une 'sous classe' issue de mongoose
import mongoose from 'mongoose';

const connectDb = () => {
    let connection = null;

    connection = mongoose.connect('mongodb://localhost:27017/blog', {
        useNewUrlParser: true,
        // useUnifiedTopology: true
    });
    return connection;
};

export default {connectDb};
